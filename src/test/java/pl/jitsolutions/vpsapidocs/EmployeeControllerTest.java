package pl.jitsolutions.vpsapidocs;

import static org.mockito.Mockito.when;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static com.epages.restdocs.WireMockDocumentation.wiremockJson;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.restdocs.RestDocsMockMvcConfigurationCustomizer;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentationConfigurer;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import pl.jitsolutions.vpsapidocs.controller.EmployeeController;
import pl.jitsolutions.vpsapidocs.model.Employee;
import pl.jitsolutions.vpsapidocs.repository.EmployeeRepository;
import java.util.Arrays;

@RunWith(SpringRunner.class)
@WebMvcTest(EmployeeController.class)
@AutoConfigureRestDocs
public class EmployeeControllerTest {

    @MockBean
    EmployeeRepository repository;

    @Autowired
    MockMvc mockMvc;

    private ObjectMapper mapper = new ObjectMapper();

    @TestConfiguration
    static class CustomizationConfiguration implements RestDocsMockMvcConfigurationCustomizer {

        @Override
        public void customize(MockMvcRestDocumentationConfigurer configurer) {
            configurer.operationPreprocessors()
                    .withRequestDefaults(prettyPrint())
                    .withResponseDefaults(prettyPrint());
        }

        @Bean
        public RestDocumentationResultHandler restDocumentation() {
            return MockMvcRestDocumentation.document("{method-name}",preprocessRequest(prettyPrint()),
                    preprocessResponse(prettyPrint()),
                    wiremockJson());
        }
    }

    @Before
    public void setUp() {
        Employee employee1 = new Employee(1L, 1L, "John Smith", 33, "Developer");
        employee1.setId(1L);
        Employee employee2 = new Employee(2L, 1L, "Adam Brown", 23, "Tester");
        employee2.setId(2L);
        when(repository.add(Mockito.any(Employee.class))).thenReturn(employee1);
        when(repository.findById(1L)).thenReturn(employee1);
        when(repository.findAll()).thenReturn(Arrays.asList(employee1, employee2));
    }

    @Test
    public void addPerson() throws Exception {
        Employee employee = new Employee(1L, 1L, "John Smith", 33, "Developer");
        mockMvc.perform(
                post("/api/employees")
                        .header("Authorization", "vps-access-token").
                        contentType(MediaType.APPLICATION_JSON).
                        content(mapper.writeValueAsString(employee))
        ).andExpect(status().isOk())
                .andDo(document("{method-name}/",
                        wiremockJson(),
                        requestHeaders(
                                headerWithName("Authorization").description(
                                        "Basic auth credentials"),
                                headerWithName("Content-Type").description(
                                        "Content type")),
                        requestFields(
                                fieldWithPath("id").description("Employee id").ignored(),
                                fieldWithPath("organizationId").description("Employee's organization id"),
                                fieldWithPath("departmentId").description("Employee's department id"),
                                fieldWithPath("name").description("Employee's name"),
                                fieldWithPath("age").description("Employee's age"),
                                fieldWithPath("position").description("Employee's position inside organization")
                        )));
    }

    @Test
    public void findPersonById() throws Exception {
        this.mockMvc.perform(get("/api/employees/{id}", 1).header("Authorization", "vps-access-token").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("{method-name}/",
                        requestHeaders(
                                headerWithName("Authorization").description(
                                        "Basic auth credentials"),
                                headerWithName("Content-Type").description(
                                        "Content type")),
                        responseFields(
                                fieldWithPath("id").description("Employee id"),
                                fieldWithPath("organizationId").description("Employee's organization id"),
                                fieldWithPath("departmentId").description("Employee's department id"),
                                fieldWithPath("name").description("Employee's name"),
                                fieldWithPath("age").description("Employee's age"),
                                fieldWithPath("position").description("Employee's position inside organization")
                        ),
                        pathParameters(parameterWithName("id").description("Employee id"))));
    }

    @Test
    public void findAllPersons() throws Exception {
        this.mockMvc.perform(get("/api/employees").header("Authorization", "vps-access-token").contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(document("{method-name}/",
                        requestHeaders(
                                headerWithName("Authorization").description(
                                        "Basic auth credentials"),
                                headerWithName("Content-Type").description(
                                        "Content type")),
                        responseFields(
                                fieldWithPath("[]").description("List of employees"),
                                fieldWithPath("[].id").description("Employee id"),
                                fieldWithPath("[].organizationId").description("Employee's organization id"),
                                fieldWithPath("[].departmentId").description("Employee's department id"),
                                fieldWithPath("[].name").description("Employee's name"),
                                fieldWithPath("[].age").description("Employee's age"),
                                fieldWithPath("[].position").description("Employee's position inside organization")
                        )));
    }


}
