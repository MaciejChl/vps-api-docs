= API Specification
{project-version}
:doctype: book
:icons: font
:source-highlighter: highlightjs
:toc: left
:toclevels: 4
:sectlinks:
:sectnums:

== Add new person

A `POST` request is used to add a new person

operation::add-person[snippets='request-headers,request-fields,curl-request,http-request,http-response']

== Find person by id

A `GET` request is used to find a person by id

operation::find-person-by-id[snippets='request-headers,path-parameters,curl-request,http-request,http-response,response-fields']


== Find all persons

A `GET` request is used to find all persons

operation::find-all-persons[snippets='request-headers,curl-request,http-request,http-response,response-fields']