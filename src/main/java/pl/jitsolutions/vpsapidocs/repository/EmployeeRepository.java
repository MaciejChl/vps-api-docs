package pl.jitsolutions.vpsapidocs.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import pl.jitsolutions.vpsapidocs.model.Employee;

public class EmployeeRepository {

	private List<Employee> employees = new ArrayList<>();
	
	public Employee add(Employee employee) {
		employee.setId((long) (employees.size()+1));
		employees.add(employee);
		return employee;
	}
	
	public Employee findById(Long id) {
		Optional<Employee> employee = employees.stream().filter(a -> a.getId().equals(id)).findFirst();
		return employee.orElse(null);
	}
	
	public List<Employee> findAll() {
		return employees;
	}

}
