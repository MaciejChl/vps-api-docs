package pl.jitsolutions.vpsapidocs.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.jitsolutions.vpsapidocs.model.Employee;
import pl.jitsolutions.vpsapidocs.repository.EmployeeRepository;

@Configuration
public class AppConfig {

    @Bean
    EmployeeRepository repository() {
        EmployeeRepository repository = new EmployeeRepository();
        repository.add(new Employee(1L, 1L, "John Smith", 34, "Analyst"));
        repository.add(new Employee(1L, 1L, "Darren Hamilton", 37, "Manager"));
        repository.add(new Employee(1L, 1L, "Tom Scott", 26, "Developer"));
        return repository;
    }

}
