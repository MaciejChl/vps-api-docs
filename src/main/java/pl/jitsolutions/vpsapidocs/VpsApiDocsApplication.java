package pl.jitsolutions.vpsapidocs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VpsApiDocsApplication {
	public static void main(String[] args) {
		SpringApplication.run(VpsApiDocsApplication.class, args);
	}

}
